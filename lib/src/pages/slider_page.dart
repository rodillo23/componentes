import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {

  _SliderPageState createState() => _SliderPageState();

}

class _SliderPageState extends State<SliderPage> {
  
  double _valorSlider = 200.0;
  bool _bloquearSlider = false;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider Page'),
      ),

      body: Container(
        padding: EdgeInsets.only(top: 50.0),
        child: Column(
          children: <Widget>[
            _createSlider(),
            _createCheckbox(),
            _createSwitch(),
            Expanded(
              child: _createImage()
            )
          ],
        ),
      ),
    );
  }

  Widget  _createSlider() {
    return Slider(
      activeColor: Colors.indigoAccent,
      //divisions: 20,
      inactiveColor: Colors.orangeAccent,
      label: "Tamaño de la imagen ",  
      min: 100.0,
      max: 400.0,
      value: _valorSlider,
      onChanged: (_bloquearSlider) ? null : (valor){
        setState(() {
        _valorSlider = valor;
        print(valor);
        });
      },
    );
  }

  Widget _createCheckbox(){
    // return Checkbox(
    //   value: _bloquearSlider,
    //   onChanged: (valor){
    //     setState(() {
    //       _bloquearSlider = valor;
    //     });
    //   },
    // );

    return CheckboxListTile(
      title: Text('Bloquear Slider'),
      value: _bloquearSlider,
      onChanged: (valor){
        setState(() {
          _bloquearSlider = valor;
        });
      },
    );
  }

  Widget _createSwitch(){
    return SwitchListTile(
      title: Text('Bloquear slider switch'),
      value: _bloquearSlider,
      onChanged: (valor){
        setState(() {
          _bloquearSlider = valor;
        });
      },
    );
  }

  Widget _createImage(){
    return Image(
      image: AssetImage("assets/thor.png"),
      width: _valorSlider,
      fit: BoxFit.contain,
    );
  }
} 

